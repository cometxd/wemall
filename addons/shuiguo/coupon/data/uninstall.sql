/* 
* @Author: qingyuexi
* @Date:   2016-10-04 14:24:05
* @Last Modified by:   qingyuexi
* @Last Modified time: 2016-02-18 15:11:31
*/
DROP TABLE IF EXISTS `addon_coupon_menu`;
DROP TABLE IF EXISTS `addon_coupon`;
DROP TABLE IF EXISTS `addon_coupon_change`;
DROP TABLE IF EXISTS `addon_coupon_active_config`;
DROP TABLE IF EXISTS `addon_coupon_user_log`;
